# README #

HI

### What is this repository for? ###

* Working on Drupal Commons locally
* Version 7.x-3.32
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* git clone into your chosen directory
* navigate to d7-commons-lamp and vagrant up
* will install into devdb database, with devdb user and devdb pass
* forked from https://github.com/mwalters/simple-vagrant-lamp
* added some drupal commons specific config, added git and drush from Hessam.
* specific things you have to do to get this running...
* 1. edit vagrant file:
* change config.vm.network :private_network, ip: "192.168.56.113" to whatever IP you want to use locally.
* 2. edit your local hosts file if you want to assign a domain to the IP you set.
* 3. browse to the ip or domain you set and you should see the Drupal commons install page.



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact